Ansible role for installing a CLIF server
=========================================

![CLIF](https://clif.ow2.io/clif-legacy/images/clif_48.png "CLIF is a Load Injection Framework")  
[clif.ow2.io](https://clif.ow2.io/)

clif-server role
----------------

This role installs a CLIF server runtime and its dependencies (unzip and OpenJDK8) on CentOS/Red Hat Enterprise and Debian/Ubuntu environments.

In details:
- create a clif user
- install an OpenJDK and unzip 
- download and unzip a CLIF distribution into clif user's home directory

See sample playbook local.yml (installs CLIF on localhost - must be run with superuser priviledges)  
``$ ansible-playbook local.yml ``
